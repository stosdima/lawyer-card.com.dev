<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h3>Мы находимся по адресу</h3>
                @foreach($settings as $setting)
                    @if($setting->name == 'address')
                        <div><i class="fa fa-map-marker" aria-hidden="true"></i>{{$setting->value}}</div>
                    @endif
                @endforeach
            </div>
            <div class="col-md-3">
                <h3>Контакты</h3>
                @foreach($settings as $setting)
                    @if($setting->name == 'phone')
                        <div><i class="fa fa-mobile" aria-hidden="true"></i> {{$setting->value}}</div>
                    @endif
                @endforeach
            </div>

            <div class="col-md-3">
                <h3>Ссылки</h3>
                @foreach($settings as $setting)
                    @if($setting->name == 'client_email')
                        <div><span>Для клиентов:</span> {{$setting->value}}</div>
                    @elseif($setting->name == 'work_email')
                        <div class="forPartners"><a href="#partnershipForm">Для сотрудничества:</a>  {{$setting->value}}</div>
                    @endif
                @endforeach
                <div></div>
            </div>
            <div class="col-md-3">
                <h3>Мы в соцсетях</h3>
                @foreach($settings as $setting)
                    @if($setting->name == 'social_vk')
                        <span><a href="{{$setting->value}}"><i class="fa fa-vk" aria-hidden="true"></i></a></span>
                    @elseif($setting->name == 'social_fb')
                        <span><a href="{{$setting->value}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                    @elseif($setting->name == 'social_inst')
                        <span><a href="{{$setting->value}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></span><br>
                    @endif
                @endforeach
                <a class="go-up" href="#adv_top"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</footer>