<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Мы в соцсетях</h3>
                <span><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></span>
                <span><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                <span><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></span><br>
            </div>
        </div>
    </div>
</footer>