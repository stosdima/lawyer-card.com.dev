<header>
    <a href="/"><img src="{!! asset('img/logo.png') !!}" alt="Expert Service"></a>
    <nav class="top-menu">
        <button class="main-menu-button"><i class="fa fa-bars" aria-hidden="true"></i></button>
        <ul class="dropdown">
            <li class="dropdown-top">
                <a class="dropdown-top" href="/">Главная</a></li>
            <li class="dropdown-top">
                <a class="dropdown-top" id="country" href="#">Страны</a>
                <ul class="dropdown-inside">
                    <li><a href="#">Польша</a></li>
                    <li><a href="#">Чехия</a></li>
                </ul>
            </li>
            <li class="dropdown-top"><a class="dropdown-top" href="{{route('services.all')}}">Услуги</a></li>
            <li class="dropdown-top"><a class="dropdown-top" href="{{route('vacancy.all')}}">Вакансии</a></li>
            <li class="dropdown-top"><a class="dropdown-top" href="{{route('comment.all')}}">Отзывы</a></li>
            <li class="dropdown-top"><a class="dropdown-top" href="{{route('contact.all')}}">Контакты</a></li>
        </ul>
    </nav>
    <div class="phone-box">
        @foreach($settings as $setting)
            @if($setting->name == 'phone')
                <div><i class="fa fa-mobile" aria-hidden="true"></i> {{$setting->value}}</div>
            @endif
        @endforeach
        <button class="rcall" href="#form">Заказать обратный звонок</button>
    </div>
</header>
<div class="fixed_after" id="adv_top"></div>
@if(Session::has('callback'))
    <div class="alert alert-info">
        {{ Session::get('callback') }}
    </div>
@endif