@extends('layouts.main')
@section('content')
    <section class="vacancy-content">
	<div class="container">
		<div class="row">
			{!! Form::open(['route' => 'vacancy.search', 'method' => 'POST', 'id' => 'vacancyForm']) !!}

				<fieldset>
				<legend>Поиск вакансий</legend>
				<p>
						<span class="col-lg-3 col-md-6 option-item">
							{!! Form::label('Категория:') !!}
							{!! Form::select('category', ['',
								'Для мужчин' => 'Для мужчин',
								'Для женщин' =>'Для женщин',
								'Для пар' => 'Для пар',
								'Специалисты' => 'Специалисты']) !!}
						</span>
					<span class="col-lg-3 col-md-6 option-item">
						{!! Form::label('Страна:') !!}
						{!! Form::select('country', [
							'',
							 'Польша' => 'Польша',
							  'Чехия' => 'Чехия']) !!}
						</span>
					<span class="col-lg-3 col-md-6 option-item">
						{!! Form::label('Возраст:') !!}
						{!! Form::select('age', ['',
							 'От 18 лет' => 'От 18 лет',
							  'От 20 лет' => 'От 20 лет',
							   'До 40 лет' => 'До 40 лет']) !!}
						</span>
					<span class="col-lg-3 col-md-6 option-item">
						{!! Form::label('Опыт работы:') !!}
						{!! Form::select('expirience', ['',
							 'Без опыта' => 'Без опыта',
							  '1-5 лет' => '1-5 лет',
							   '5-10 лет' => '5-10 лет',
								'10-15 лет' => '10-15 лет',
								'15-20 лет' => '15-20 лет']) !!}
						</span>
				</p>
				<p>
				<div class="col-lg-12 checkbox-item">
					{!! Form::label('Наличие сертификатов/дипломов') !!}
					{!! Form::checkbox('certificate', 1) !!}
				</div>
				</p>
				<p>
				<div class="col-lg-12 lang-item">
					<span class="col-lg-3 col-md-6 option-item">
						{!! Form::label('Язык: ') !!}
						{!! Form::select('lang_name',$langName->prepend('', '0') ) !!}
					</span>
						<span class="col-lg-3 col-md-6 option-item">
						{!! Form::label('Уровень владения: ') !!}
						{!! Form::select('lang_knowledge', ['',
							  'Начальный' => 'Начальный',
							  'Средний' => 'Средний',
							  'Продвинутый' => 'Продвинутый'
												])!!}
					</span>

				</div>

				</p>
				<button class="col-lg-3 col-md-12 col-sm-12 col-xs-12">Поиск</button>
			</fieldset>
			{!! Form::close() !!}

			<div class="row">
				@if(isset($error))
					{{$error}}
				@endif
				@foreach($vacancies as $vacancy)
					<div class="col-lg-4 col-md-6">
						<div class="vacancy-item">
							<h2>{{$vacancy->name}}</h2>
							<div class="vi vi-category"><b><u>Категория:</u></b> {{$vacancy->category}}</div>
							<div class="vi vi-age"><b><u>Возраст:</u></b> {{$vacancy->age}}</div>
							<div class="vi vi-stage"><b><u>Опыт работы:</u></b> {{$vacancy->expirience}}</div>
							<div class="vi vi-age"><b><u>Наличие сертефикатов/дипломов:</u></b>
								@if($vacancy->sertificate == '0')
									Нет
								@else
									Да
								@endif
							</div>
							<div class="vi vi-age"><b><u>Знание языка({{$vacancy->lang_name}}):</u></b> {{$vacancy->lang_knowledge}}</div>
							<div class="vacancy-text"> {{$vacancy->description}}</div>
							<button href="#form">Подробнее</button>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
@stop