@extends('layouts.main')
@section('content')
	<section class="single-article-content">
				<div class="container">
					<div class="row">
						<article>
							<h2>{{$post->title}}</h2>
							<h4><u>Ключевые слова:</u>
                                <?php $tagsArray =[]; ?>
								@foreach($post->tags as $tag)
                                    <?php $tagsArray[] = $tag->name ?>
								@endforeach
									{{implode(',', $tagsArray)}}
							</h4>
							<div class="article-body">
								{!! $post->body !!}
								<p>
									<b><u>Читайте также:</u></b><br>
									<ul>
										@foreach($readMore as $post)
											<li><a href="{{ route('post.show', $post->id) }}">{{$post->title}}</a></li>
										@endforeach
									</ul>
								</p>
							</div>
						</article>
					</div>
				</div>
			</section>
@stop