@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row all-reviews">
            <h1>ОТЗЫВЫ О КОМПАНИИ "EXPERT SERVICE", НИКОЛАЕВ</h1>
            <div class="send-review">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p >{{ $message }}</p>
                    </div>
                @endif
                {!! Form::open(['route' => 'comment.store', 'method' => 'POST', 'class' => 'send-review-form']) !!}
                <div class="col-lg-6">
                    {{Form::textarea('body', null, ['cols' => '30', 'rows' => '6', 'placeholder' => 'Ваш отзыв'])}}
                </div>
                <div class="col-lg-6">
                    {{Form::text('full_name', null, ['placeholder' => 'Имя и Фамилия*'])}}
                    {{Form::text('social_link', null, ['placeholder' => 'Ссылка на страницу соц. сети'])}}
                    {{Form::submit('Оставить отзыв')}}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="clearfix bottom-line"></div>
            <?php $counter = 0; ?>
            @foreach($comments as $comment)
                @if($comment->approve == true)
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="review-item">
                            <div class="review-text">{{$comment->body}}</div>
                            <div class="review-name">{{$comment->full_name}}</div>
                            <div class="review-link">{{$comment->social_link}}</div>
                        </div>
                    </div>
                    <?php $counter++ ; ?>
                    @if($counter % 4 == 0)
                        <div class="clearfix"></div>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
@stop