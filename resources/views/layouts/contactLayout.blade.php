<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->

<head>

    <meta charset="utf-8">

    <title>Expert Service</title>
    <meta name="description" content="">

    <link rel="shortcut icon" href="{!! asset('img/favicon/favicon.ico') !!}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{!! asset('img/favicon/apple-touch-icon.png')!!}">
    <link rel="apple-touch-icon" sizes="72x72" href="{!! asset('img/favicon/apple-touch-icon-72x72.png') !!}">
    <link rel="apple-touch-icon" sizes="114x114" href="{!! asset('img/favicon/apple-touch-icon-114x114.png') !!}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{!! asset('libs/bootstrap/css/bootstrap-grid.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('libs/animate/animate.css') !!}">
    <link rel="stylesheet" href="{!! asset('libs/font-awesome/css/font-awesome.min.css') !!}">

    <link rel="stylesheet" href="{!! asset('css/fonts.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/header.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/magnific-popup.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/main.css') !!}">

    <script src="{!! asset('libs/modernizr/modernizr.js') !!}"></script>

    <style>
        #popup__toggle{bottom:25px;right:10px;position:fixed;}
        .img-circle{background-color:#29AEE3;box-sizing:content-box;-webkit-box-sizing:content-box;}
        .circlephone{box-sizing:content-box;-webkit-box-sizing:content-box;border: 2px solid #29AEE3;width:150px;height:150px;bottom:-25px;right:10px;position:absolute;-webkit-border-radius:100%;-moz-border-radius: 100%;border-radius: 100%;opacity: .5;-webkit-animation: circle-anim 2.4s infinite ease-in-out !important;-moz-animation: circle-anim 2.4s infinite ease-in-out !important;-ms-animation: circle-anim 2.4s infinite ease-in-out !important;-o-animation: circle-anim 2.4s infinite ease-in-out !important;animation: circle-anim 2.4s infinite ease-in-out !important;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        .circle-fill{box-sizing:content-box;-webkit-box-sizing:content-box;background-color:#29AEE3;width:100px;height:100px;bottom:0px;right:35px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;-webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;-moz-animation: circle-fill-anim 2.3s infinite ease-in-out;-ms-animation: circle-fill-anim 2.3s infinite ease-in-out;-o-animation: circle-fill-anim 2.3s infinite ease-in-out;animation: circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        .img-circle{box-sizing:content-box;-webkit-box-sizing:content-box;width:72px;height:72px;bottom: 14px;right: 49px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;opacity: .7;}
        .img-circleblock{box-sizing:content-box;-webkit-box-sizing:content-box;width:72px;height:72px;background-image:url(img/mini.png);background-position: center center;background-repeat:no-repeat;animation-name: tossing;-webkit-animation-name: tossing;animation-duration: 1.5s;-webkit-animation-duration: 1.5s;animation-iteration-count: infinite;-webkit-animation-iteration-count: infinite;}
        .img-circle:hover{opacity: 1;}
        @keyframes pulse {0% {transform: scale(0.9);opacity: 1;}
            50% {transform: scale(1); opacity: 1; }
            100% {transform: scale(0.9);opacity: 1;}}
        @-webkit-keyframes pulse {0% {-webkit-transform: scale(0.95);opacity: 1;}
            50% {-webkit-transform: scale(1);opacity: 1;}
            100% {-webkit-transform: scale(0.95);opacity: 1;}}
        @keyframes tossing {
            0% {transform: rotate(-8deg);}
            50% {transform: rotate(8deg);}
            100% {transform: rotate(-8deg);}}
        @-webkit-keyframes tossing {
            0% {-webkit-transform: rotate(-8deg);}
            50% {-webkit-transform: rotate(8deg);}
            100% {-webkit-transform: rotate(-8deg);}}
        @-moz-keyframes circle-anim {
            0% {-moz-transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;-moz-opacity: .1;-webkit-opacity: .1;-o-opacity: .1;}
            30% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;-moz-opacity: .5;-webkit-opacity: .5;-o-opacity: .5;}
            100% {-moz-transform: rotate(0deg) scale(1) skew(1deg);opacity: .6;-moz-opacity: .6;-webkit-opacity: .6;-o-opacity: .1;}}
        @-webkit-keyframes circle-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity: .1;}
            30% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);-webkit-opacity: .5;}
            100% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);-webkit-opacity: .1;}}
        @-o-keyframes circle-anim {
            0% {-o-transform: rotate(0deg) kscale(0.5) skew(1deg);-o-opacity: .1;}
            30% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);-o-opacity: .5;}
            100% {-o-transform: rotate(0deg) scale(1) skew(1deg);-o-opacity: .1;}}
        @keyframes circle-anim {
            0% {transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;}
            30% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;}
            100% {transform: rotate(0deg) scale(1) skew(1deg);
                opacity: .1;}}
        @-moz-keyframes circle-fill-anim {
            0% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);opacity: .2;}
            100% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-webkit-keyframes circle-fill-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;  }
            50% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;  }
            100% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-o-keyframes circle-fill-anim {
            0% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-o-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @keyframes circle-fill-anim {
            0% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    </style>

</head>

<body>
<a href="#form" id="popup__toggle" onclick="return false;" style="z-index: 9999"><div class="circlephone" style="transform-origin: center;"></div><div class="circle-fill" style="transform-origin: center;"></div><div class="img-circle" style="transform-origin: center;"><div class="img-circleblock" style="transform-origin: center;"></div></div></a>

@include('partials.header')
@yield('content')
@include('partials.footer2')

<div class="hidden">
    {{--Callback form--}}
    {!! Form::open(['route' => 'callback.store', 'id' => 'form', 'method' => 'POST']) !!}

    {!! Form::label('Ваше имя:') !!}<br>
    {!! Form::text('name', null, ['placeholder' => 'Введите имя']) !!}<br>

    {!! Form::label('Ваш телефон:') !!}<br>
    {!! Form::text('phone', null, ['placeholder' => 'Введите телефон']) !!}<br>

    <button class="submit">Перезвоните мне</button>
    {!! Form::close() !!}
    {{--Coop form--}}
    {!! Form::open(['route' => 'callback-coop.store', 'id' => 'partnershipForm', 'method' => 'POST']) !!}

    {!! Form::label('Ваше название::') !!}<br>
    {!! Form::text('name', null, ['placeholder' => 'Компания/Агенство*']) !!}<br>

    {!! Form::label('Ваши контактные данные:') !!}<br>
    {!! Form::text('info', null, ['placeholder' => 'Введите телефон']) !!}<br>

    {!! Form::label('Направление сотрудничества:') !!}<br>
    {!! Form::textarea('text', null, ['placeholder' => 'Введите телефон', 'cols' => '30', 'rows' => '5']) !!}<br>

    <button class="submit">Отправить мне</button>
    {!! Form::close() !!}
</div>

<div class="loader">
    <div class="loader_inner"></div>
</div>

<!--[if lt IE 9]>
<script src="{!! asset('libs/html5shiv/es5-shim.min.js') !!}"></script>
<script src="{!! asset('libs/html5shiv/html5shiv.min.js') !!}"></script>
<script src="{!! asset('libs/html5shiv/html5shiv-printshiv.min.js') !!}"></script>
<script src="{!! asset('libs/respond/respond.min.js') !!}"></script>
<![endif]-->

<script src="{!! asset('libs/jquery/jquery-1.11.2.min.js') !!}"></script>
<script src="{!! asset('libs/waypoints/waypoints.min.js') !!}"></script>
<script src="{!! asset('libs/animate/animate-css.js') !!}"></script>
<script src="{!! asset('libs/plugins-scroll/plugins-scroll.js') !!}"></script>
<script src="{!! asset('js/jquery.magnific-popup.min.js') !!}"></script>
<script src="{!! asset('js/jquery.countdown.min.js') !!}"></script>
<script src="{!! asset('js/readmore.min.js') !!}"></script>
<script src="{!! asset('js/common.js') !!}"></script>

</body>
</html>