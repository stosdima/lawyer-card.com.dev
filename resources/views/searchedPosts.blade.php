@extends('layouts.main')
@section('content')
    <section class="articles-content">
        <div class="container">
            <div class="row">
                {!! Form::open(['method' => 'POST', 'route' => 'post.search', 'role' => 'search']) !!}
                <div class="col-lg-4 col-md-5">
                    <span>Поиск статей по ключевым словам:</span>
                </div>
                <div class="col-lg-5 col-md-4">
                    {{Form::text('keyword', null, ['placeholder' => 'Введите ключевое слово'])}}
                </div>
                <div class="col-lg-3 col-md-3">
                    <button class="search-btn">Поиск</button>
                </div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                @if(isset($error))
                    {{$error}}
                @endif
                @foreach($searchedPosts as $post)
                    <div class="article-item">
                        <div class="row">
                            <div class="col-md-8">
                                <h2><a href="{{ route('post.show', $post->id) }}">{{$post->title}}</a></h2>
                                <span> <strong>Ключевые слова:</strong></span>
                                <?php $tagsArray =[]; ?>
                                @foreach($post->tags as $tag)
                                    <?php $tagsArray[] = $tag->name ?>
                                @endforeach
                                <span> {{implode(',', $tagsArray)}}</span>
                            </div>
                            <div class="col-md-4 txt-right">
                                <a href="{{ route('post.show', $post->id) }}" class="read-article">Подробнее</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- Load Scripts Start -->
    {{--	<script>var scr = {"scripts":[
            {"src" : "js/libs.js", "async" : false},
            {"src" : "js/jquery.countdown.min.js", "async" : false},
            {"src" : "js/common.js", "async" : false}
            ]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
        </script>--}}
    <!-- Load Scripts End -->
@stop
