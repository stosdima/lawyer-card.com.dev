@extends('layouts.contactLayout')
@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <section class="main-contact-info">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="map-box">
											<span>
											<i class="fa fa-map-marker" aria-hidden="true"></i>
											<strong>Адрес:</strong> ул. Соборная 12-Б,<br>
											БЦ "Украина", оф.203
											</span>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2722.7661546094987!2d31.98697861524602!3d46.96628437914733!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c5c96431b74b5b%3A0xe1de563b8ae8e0c5!2z0JHQpiAi0KPQutGA0LDQuNC90LAi!5e0!3m2!1sru!2sua!4v1497475850133" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <ul class="all-contacts">
                                    @foreach($settings as $setting)
                                        @if($setting->name == 'phone')
                                            <li><i class="fa fa-mobile" aria-hidden="true"></i> {{$setting->value}}</li>
                                        @elseif($setting->name == 'skype')
                                            <li><i class="fa fa-skype" aria-hidden="true"></i> {{$setting->value}}</li>
                                        @elseif($setting->name == 'client_email')
                                            <li>
                                                <i class="fa fa-envelope-square" aria-hidden="true"></i>
                                                Для клиентов: {{$setting->value}}
                                            </li>
                                        @elseif($setting->name == 'work_email')
                                            <li>
                                                <i class="fa fa-envelope-square" aria-hidden="true"></i>
                                                <span class="forPartners"><a href="#partnershipForm">Для сотрудничества:</a> {{$setting->value}}</span>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="form-box">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p >{{ $message }}</p>
                                        </div>
                                    @endif
                                    <div class="form-title">Задайте нам вопрос</div>
                                    {!! Form::open(['route' => 'contact.store', 'class' => 'ask-form', 'method' => 'POST']) !!}

                                    {!! Form::label('Ваше имя:') !!}
                                    {!! Form::text('name', null, ['placeholder' => 'Введите имя', 'required' => 'required']) !!}

                                    {!! Form::label('Ваш телефон:') !!}
                                    {!! Form::text('phone', null, ['placeholder' => 'Введите телефон', 'required' => 'required']) !!}

                                    {!! Form::label('Ваша почта:') !!}
                                    {!! Form::text('email', null, ['placeholder' => 'Введите почту', 'required' => 'required']) !!}

                                    {!! Form::label('Ваш вопрос:') !!}
                                    {!! Form::textarea('question', null, ['placeholder' => 'Введите сообщение', 'required' => 'required']) !!}

                                    {!! Form::submit('Отправить') !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@stop