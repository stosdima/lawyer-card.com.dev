@extends('layouts.main')
@section('content')
	<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<section class="attention-info">
				<i class="fa fa-times" aria-hidden="true"></i>
				<h1>Внимание АКЦИЯ!</h1>
				<span class="info"></span>
				<div id="countdown"></div>
			</section>
		</div>
		<div class="col-lg-12">
			<section class="advantages" >
				<h1>Мы трудоустраиваем за границей</h1>
				<h2>5 главных преимуществ</h2>
				<div class="col-md-4"><div class="adw-box"><i class="fa fa-users" aria-hidden="true"></i><br>Более 100 компаний партнеров с проверенными вакансиями</div></div>
				<div class="col-md-4"><div class="adw-box"><i class="fa fa-clock-o" aria-hidden="true"></i><br>Выполнение поставленной задачи в срок</div></div>
				<div class="col-md-4"><div class="adw-box"><i class="fa fa-suitcase" aria-hidden="true"></i><br>Возможность путешествовать и работать</div></div>
				<div class="clearfix"></div>
				<div class="col-md-6"><div class="adw-box"><i class="fa fa-list-ol" aria-hidden="true"></i><br>Оплата услуг компании в несколько этапов</div></div>
				<div class="col-md-6"><div class="adw-box"><i class="fa fa-handshake-o" aria-hidden="true"></i><br>Только официальная работа с оформлением договора</div></div>
			</section>
		</div>
		<div class="col-md-12">
			<section class="guarantees">
				<h1>Гарантии для вас</h1>
				<div class="col-md-4">
					<div class="guarnt-box">
						<i class="fa fa-id-card-o" aria-hidden="true"></i><br> Агентство лицензированное
					</div>
				</div>
				<div class="col-md-4">
					<div class="guarnt-box">
						<i class="fa fa-handshake-o" aria-hidden="true"></i><br> Партнеры заключают с вами договор об официальном трудоустройстве сразу по приезду
					</div>
				</div>
				<div class="col-md-4">
					<div class="guarnt-box">
						<i class="fa fa-refresh" aria-hidden="true"></i><br> Вы можете воспользоваться правом перетрудоустройства
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6">
					<div class="guarnt-box">
						<i class="fa fa-file-text-o" aria-hidden="true"></i><br>Работаем с вами в Украине по договору
					</div>
				</div>
				<div class="col-md-6">
					<div class="guarnt-box">
						<i class="fa fa-reply-all" aria-hidden="true"></i></i><br>Менеджер всегда поддерживает обратную связь и готов ответить на все сопутствующие вопросы
					</div>
				</div>
			</section>
		</div>
		<div class="col-lg-4">
			<section class="article-list">
				<h3>Статьи на сайте:</h3>
				@foreach($posts as $post)
					<p><a href="{{ route('post.show', $post->id) }}">{{$post->title}}</a></p>
				@endforeach
			</section>
			<div class="all-articles"><a href="{{route('post.all')}}">Просмотреть все</a></div>
			<section class="reviews">
				@foreach($comments as $comment)
					@if($comment->approve == true)
						<div class="review">
							<h2>{{$comment->full_name}}</h2>
							<span class="review-text">{{$comment->body}}</span><br>
							<span class="review-link"><a href="{{$comment->social_link}}">{{$comment->social_link}}</a></span>
						</div>
					@endif
				@endforeach
				<div class="view-more">
					<a href="{{route('comment.all')}}">Прочесть полностью</a>
				</div>
			</section>
		</div>

		<div class="col-lg-8 col-md-12">
			<section class="main-info">
				<h1>О нашей фирме</h1>
				<p>Мы-фирма по трудоустройству за границей. Наше предприятие называется:«<em><strong>ExpertService</strong></em>». (Лицензия Министерства социальной политики № 641 от 10.06.2016 г.)</p>
				<p>Успешно ведем свою деятельность на рынке труда уже свыше 2 х лет. С 2015 года агентство «ExpertService» зарекомендовало себя как надежного партнера, как для соискателей в поиске работы за границей, так и для работодателей.</p>
				<p>На данный момент мы занимаемся оформлением пакета документов для трудоустройства за границей в направлении сотрудничества <strong>«Украина — Польша»</strong>.</p>
				<p>За период нашей деятельности была оказана помощь в открытии уже более 600 рабочих виз, среди которых множество Шенгенских виз с различными целями визита и Национальных рабочих виз, как полугодовых (180/360, 180/180), так и годовых (Воеводские 360/360).</p>
				<p>В процессе работы мы неоднократно помогли семьям с детьми переехать в Польшу на ПМЖ.</p>
				<p>Предоставляем помощь в сборе необходимых документов для людей, у которых уже есть приглашение от работодателя, или любой другой документ, подтверждающий цель визита.</p>
				<p>Наша фирма является официальным страховым агентом Страховой компании «ВУСО», поэтому цены на страховые полиса у нас идентичны с нашим партнером.</p>
				<p>Помогаем с покупкой автобусных и Ж/Д билетов.</p>
				<p>Занимаемся изготовлением заграничного паспорта.</p>
				<p>Длительный период времени с нами сотрудничают более 150 надежных партнеров в Польше.</p>
				<p>С нашей помощью было трудоустроено более, чем 300 человек, которые повторно обратились за нашими услугами. С нашими клиентами мы поддерживаем связь как на протяжении всего нашего сотрудничества, так и после его окончания, а также мы всегда находимся на связи и готовы ответить на все интересующие их вопросы.</p>
				<p>В связи с большим спросом наших граждан о возможности эмиграции, легализации и трудоустройстве в странах ЕС, на сегодняшний день, мы работаем над развитием новых направлений. Планируем открывать новые направление нашего агентства, такие как: «Украина - Чехия», «Украина - Прибалтика».</p>
				<p>Мы всегда рады новым предложениям и пожеланиям. Открыты к любому диалогу по сотрудничеству. Наша компания всегда работает над тем, чтобы стать лучшее, и переживаем что бы у наших клиентов все сложилось лучшим образом!</p>
				<p>Наша фирма работает со всеми регионами нашей страны. Мы всегда готовы помочь и посоветовать во всех интересующих вопросах. Консультация является исчерпывающей, благодаря опыту, который приобретен на практике уже свыше 2 х лет, и различными ситуациями которые происходили уже больше чем с 1000 наших клиентов (Консультация является бесплатной).</p>
				<p>Поэтому, если у Вас возникает какой либо вопрос, звоните мы всегда рады помочь! </p>

				<h1>Как мы работаем с вами</h1>
				<h2>Консультация<i class="fa fa-thumbs-up" aria-hidden="true"></i></h2>
				<ul style="list-style: none;">
					<li><i class="fa fa-check" aria-hidden="true"></i> Консультируем по наличию всех необходимых документов у Вас;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> По индивидуальным возможностям открытия визы;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Консультируем про возможные варианты трудоустройства в Польшу.</li>
				</ul><br>
				<h2>Оформление визы<i class="fa fa-handshake-o" aria-hidden="true"></i></h2>
				<ul style="list-style: none;">
					<li><i class="fa fa-check" aria-hidden="true"></i> Собираем полный пакет документов для получения польской рабочей визы;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Предоставляем запись в визовые центры;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Консультируем Вас про тонкости подачи пакета документов, нюансы собеседования в визовом центре;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Контролируем готовность Вашей визы</li>
				</ul><br>
				<h2>Подбор вакансии<i class="fa fa-list" aria-hidden="true"></i></h2>
				<ul style="list-style: none;">
					<li><i class="fa fa-check" aria-hidden="true"></i> Составляем резюме для Вашего трудоустройства;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Подбираем вакансии, соответствующие Вашему опыту работы и запросу;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Согласовываем Вашу кандидатуру у работодателя;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Назначаем дату приезда;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Составляем маршрут;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Помогаем в приобретении билетов;</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Консультируем по вопросам пересечения границы, а также возможности использования данной визы</li>
				</ul>
			</section>
		</div>
	</div>
</div>
@stop
