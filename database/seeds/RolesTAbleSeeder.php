<?php

use Illuminate\Database\Seeder;

class RolesTAbleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'name' => 'admin',
                'display_name' => 'admin',
                'description' => 'admin'
            ],
            [
                'name' => 'manager',
                'display_name' => 'manager',
                'description' => 'admin'
            ]
        ];

        foreach ($role as $key => $value) {
            \App\Role::create($value);
        }
    }
}
