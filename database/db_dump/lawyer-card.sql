-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 19 2017 г., 04:52
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `lawyer-card`
--

-- --------------------------------------------------------

--
-- Структура таблицы `action`
--

CREATE TABLE `action` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `action`
--

INSERT INTO `action` (`id`, `title`, `description`, `date`, `image`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'daddad', 'daddaddaad', '2017-07-27', 'storage/action/image/b6/595d133dcc82c.png', 1, '2017-07-04 17:32:40', '2017-07-17 09:51:06'),
(2, 'dasdasd', 'dsadasdasd', '2017-07-06', 'storage/action/image/1d/595c02865a3c5.jpg', 0, '2017-07-04 18:03:02', '2017-07-06 15:41:58');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approve` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `full_name`, `body`, `social_link`, `approve`, `created_at`, `updated_at`) VALUES
(32, 'Stos Dima', 'fanfbsbfasmbvskh.bh.masbfhka asbf jasbfjkaskjfb jashf haskjhfjkashfkjha sjkhf akshhfkjashfkjashfkj akjhsfk hakjhf akjsfjkhasfhas jkfh kjashfhaskjhfahsfhkjashfash kjfhjkash fkjas kjfhajfh fkjashfkjh askjfh jkashf jkasfh fkjhaskjfh akjshf kjashhf kjhasfkjhaskhfaskjhf kjasf kjhakjfh askjhfkjasf kjasfh asjkf haskjf kjas fkjashfkjasfkjhasjkfkjasf', 'facebook.com.ua', 1, '2017-06-22 12:34:55', '2017-07-05 14:57:48'),
(33, 'Stos Dima', 'fanfbsbfasmbvskh.bh.masbfhka asbf jasbfjkaskjfb jashf haskjhfjkashfkjha sjkhf akshhfkjashfkjashfkj akjhsfk hakjhf akjsfjkhasfhas jkfh kjashfhaskjhfahsfhkjashfash kjfhjkash fkjas kjfhajfh fkjashfkjh askjfh jkashf jkasfh fkjhaskjfh akjshf kjashhf kjhasfkjhaskhfaskjhf kjasf kjhakjfh askjhfkjasf kjasfh asjkf haskjf kjas fkjashfkjasfkjhasjkfkjasf', 'facebook.com.ua', 1, '2017-06-22 12:34:55', '2017-06-22 12:35:10'),
(34, 'Stos Dima', 'fanfbsbfasmbvskh.bh.masbfhka asbf jasbfjkaskjfb jashf haskjhfjkashfkjha sjkhf akshhfkjashfkjashfkj akjhsfk hakjhf akjsfjkhasfhas jkfh kjashfhaskjhfahsfhkjashfash kjfhjkash fkjas kjfhajfh fkjashfkjh askjfh jkashf jkasfh fkjhaskjfh akjshf kjashhf kjhasfkjhaskhfaskjhf kjasf kjhakjfh askjhfkjasf kjasfh asjkf haskjf kjas fkjashfkjasfkjhasjkfkjasf', 'facebook.com.ua', 1, '2017-06-22 12:34:55', '2017-06-22 12:35:10'),
(35, 'Stos Dima', 'fanfbsbfasmbvskh.bh.masbfhka asbf jasbfjkaskjfb jashf haskjhfjkashfkjha sjkhf akshhfkjashfkjashfkj akjhsfk hakjhf akjsfjkhasfhas jkfh kjashfhaskjhfahsfhkjashfash kjfhjkash fkjas kjfhajfh fkjashfkjh askjfh jkashf jkasfh fkjhaskjfh akjshf kjashhf kjhasfkjhaskhfaskjhf kjasf kjhakjfh askjhfkjasf kjasfh asjkf haskjf kjas fkjashfkjasfkjhasjkfkjasf', 'facebook.com.ua', 1, '2017-06-22 12:34:55', '2017-06-22 12:35:10'),
(36, 'dsadasdasd', 'dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd dasdasdasd', 'dsadasdasdasdasd', 1, '2017-06-26 12:02:31', '2017-06-26 12:02:38'),
(38, 'dasdsadsad', 'dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd dasda asd as ds as dasdasdasd', 'dsadasdasdas', 0, '2017-07-05 15:00:05', '2017-07-05 15:00:05');

-- --------------------------------------------------------

--
-- Структура таблицы `filter`
--

CREATE TABLE `filter` (
  `id` int(10) UNSIGNED NOT NULL,
  `filter_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `fundamental_settings`
--

CREATE TABLE `fundamental_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `fundamental_settings`
--

INSERT INTO `fundamental_settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'phone', '+380634510145', '2017-06-21 15:06:31', '2017-06-21 15:06:31'),
(2, 'phone', '+380961001970', '2017-06-21 15:06:55', '2017-06-21 15:06:55'),
(3, 'phone', '+380501004867', '2017-06-21 15:07:24', '2017-06-21 15:07:24'),
(4, 'address', 'г.Николаев, ул. Соборная 12-Б, БЦ \"Украина\", оф.203, 220', '2017-06-21 16:15:41', '2017-07-05 13:19:35'),
(5, 'client_email', 'expertnwork@gmail.com', '2017-06-21 16:31:44', '2017-06-21 16:31:44'),
(6, 'work_email', 'expertservicen@gmail.com', '2017-06-21 16:31:59', '2017-06-21 16:31:59'),
(7, 'social_vk', 'https://vk.com/expertservicen', '2017-06-21 16:37:24', '2017-06-21 17:44:04'),
(8, 'social_fb', 'https://www.facebook.com/expertserviceN', '2017-06-21 16:37:46', '2017-06-21 17:43:40'),
(9, 'social_inst', 'https://www.instagram.com/es_workpoland/', '2017-06-21 16:38:02', '2017-06-21 17:44:18'),
(10, 'skype', 'asdsadas', '2017-06-22 15:54:50', '2017-06-22 16:02:23'),
(11, 'email_notification', 'stosdima@gmail.com', '2017-07-17 16:25:42', '2017-07-17 16:25:42');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_05_29_161601_create_post_table', 3),
(7, '2014_01_07_073615_create_tagged_table', 4),
(8, '2014_01_07_073615_create_tags_table', 4),
(9, '2016_06_29_073615_create_tag_groups_table', 4),
(10, '2016_06_29_073615_update_tags_table', 4),
(11, '2017_06_01_141045_entrust_setup_tables', 5),
(14, '2017_06_03_104106_create_comments_table', 7),
(21, '2017_06_01_144407_create_fundamental_settings_table', 9),
(22, '2017_06_05_133737_entrust_setup_tables', 10),
(30, '2017_06_03_154542_create_table_actions', 12),
(33, '2017_05_28_200611_create_vacancy_table', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('stosdima@gmail.com', '$2y$10$UH0GfvaSAgzSePJF.twiSeq9HmSDFzz6qm22dXiJYyXD9OnEzEbHy', '2017-06-07 08:54:50'),
('stosdimka@gmail.com', '$2y$10$Y.//OWvsH87sD9bNnsqJh.0Uc6onCT/GxFLlnDQfE4H4KY40yKVra', '2017-07-05 11:39:51');

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(9, 'settings-list', 'Display Role Listing', 'See only Listing Of Role', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(10, 'role-create', 'Create Role', 'Create New Role', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(11, 'role-edit', 'Edit Role', 'Edit Role', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(12, 'role-delete', 'Delete Role', 'Delete Role', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(13, 'manager-list', 'Display Item Listing', 'See only Listing Of Item', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(14, 'item-create', 'Create Item', 'Create New Item', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(15, 'item-edit', 'Edit Item', 'Edit Item', '2017-06-07 09:02:07', '2017-06-07 09:02:07'),
(16, 'item-delete', 'Delete Item', 'Delete Item', '2017-06-07 09:02:07', '2017-06-07 09:02:07');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(9, 3),
(13, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(35, 'KAMA', '<p>PULYA</p>', '2017-06-10 10:23:01', '2017-06-10 10:23:01'),
(36, 'dsada', '<p>sdasdasd</p>', '2017-06-10 10:24:15', '2017-06-10 10:24:15'),
(37, 'test', '<p>teste</p>', '2017-06-28 09:24:01', '2017-06-28 09:24:01'),
(39, 'dasdasdas', '<p>dsadasdasdasdasdasdasdasdasdasd</p>\r\n\r\n<p>dasdasdasdasdasdasd</p>\r\n\r\n<p>adasdasdasdasdasdasdasdasdassd</p>\r\n\r\n<p><img alt=\"dadadada\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/PM5544_with_non-PAL_signals.png/320px-PM5544_with_non-PAL_signals.png\" style=\"height:240px; width:320px\" /></p>\r\n\r\n<p>dadddaaddaaddadaadaddaadda</p>\r\n\r\n<p><img alt=\"dadadada\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/PM5544_with_non-PAL_signals.png/320px-PM5544_with_non-PAL_signals.png\" style=\"height:240px; width:320px\" /></p>', '2017-07-05 13:17:52', '2017-07-05 13:17:52'),
(40, 'dasdasda', '<p>dsadasdasdsads</p>', '2017-07-06 15:50:31', '2017-07-06 15:50:31'),
(41, '12421124', '<p>12412412</p>', '2017-07-06 16:17:04', '2017-07-06 16:17:04');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'admin', 'admin', '2017-06-07 09:02:24', '2017-06-07 09:02:24'),
(4, 'manager', 'manager', 'manager', '2017-06-07 09:02:24', '2017-06-07 09:02:24');

-- --------------------------------------------------------

--
-- Структура таблицы `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(6, 3),
(25, 4),
(26, 4),
(27, 4),
(28, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `tagging_tagged`
--

CREATE TABLE `tagging_tagged` (
  `id` int(10) UNSIGNED NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tagging_tagged`
--

INSERT INTO `tagging_tagged` (`id`, `taggable_id`, `taggable_type`, `tag_name`, `tag_slug`) VALUES
(46, 36, 'App\\Post', '3123123123', '3123123123'),
(49, 37, 'App\\Post', 'Test1', 'test1'),
(50, 37, 'App\\Post', 'Test2', 'test2'),
(51, 37, 'App\\Post', 'Test3', 'test3'),
(54, 39, 'App\\Post', 'Test', 'test'),
(59, 40, 'App\\Post', 'Dsadsadasdasdasdas', 'dsadsadasdasdasdas'),
(61, 35, 'App\\Post', 'Dasd', 'dasd'),
(63, 41, 'App\\Post', '1', '1'),
(64, 35, 'App\\Post', 'Фывфывфыв', 'fyvfyvfyv');

-- --------------------------------------------------------

--
-- Структура таблицы `tagging_tags`
--

CREATE TABLE `tagging_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_group_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggest` tinyint(1) NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tagging_tags`
--

INSERT INTO `tagging_tags` (`id`, `tag_group_id`, `slug`, `name`, `suggest`, `count`) VALUES
(14, NULL, 'test', 'Test', 0, 1),
(19, NULL, '3123123123', '3123123123', 0, 1),
(22, NULL, 'test1', 'Test1', 0, 1),
(23, NULL, 'test2', 'Test2', 0, 1),
(24, NULL, 'test3', 'Test3', 0, 1),
(26, NULL, 'dsadsadasdasdasdas', 'Dsadsadasdasdasdas', 0, 1),
(28, NULL, 'dasd', 'Dasd', 0, 1),
(30, NULL, '1', '1', 0, 1),
(31, NULL, 'fyvfyvfyv', 'Фывфывфыв', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tagging_tag_groups`
--

CREATE TABLE `tagging_tag_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'stosdimka', 'stosdimka@gmail.com', '$2y$10$x0InGYVnetPkHTlw0FdJwu5itXKcJwrNqL1iETa6QlaVi3mlR09.u', 'zVkfEQ9idsBPbPtkr0VDRroAiTx6UZOMu1YnvepR60ZIvtViVjg2wesc5F5L', '2017-06-05 21:15:07', '2017-06-05 21:15:07'),
(25, 'dasdssd', 'dsadasd@mail.ru', '$2y$10$vBXtUR5C.S.43s9.c0DA0u3UFw.zgheWDrkFKWQJegxjbJjIJTtuO', NULL, '2017-06-10 12:15:26', '2017-06-10 12:15:34'),
(26, 'dsadad', 'dasdasdasdasd@mail.ru', '$2y$10$S0ZzvSUHrjdUpsXxjoilfempl3UvszHHIS/6luCTZgbogPqzo4yg2', NULL, '2017-06-11 08:13:44', '2017-06-11 08:13:44'),
(27, 'test', 'test@mail.ru', '$2y$10$UgnL1HmXPe13tTohed.RHuBlv6hS82232FCQvXSGiFyBhxh6FBkgK', '5jBy7VyCctJOUjgxj5IFe4zZgqg8maRuvMBnGUDxfDVZ1ioaw4ZnFOebU1O2', '2017-06-12 17:35:55', '2017-06-12 17:36:49'),
(28, 'dasdasd', 'dsadasdsaA@mail.ru', '$2y$10$ZoW5sp.01o3caDuqtXyxfOYUGUXd6kSGUq3mucVWLPJjQUrPvwsu6', NULL, '2017-07-12 01:32:14', '2017-07-12 01:32:14');

-- --------------------------------------------------------

--
-- Структура таблицы `vacancy`
--

CREATE TABLE `vacancy` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_knowledge` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertificate` tinyint(1) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `action_date_unique` (`date`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fundamental_settings`
--
ALTER TABLE `fundamental_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `tagging_tagged`
--
ALTER TABLE `tagging_tagged`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tagged_taggable_id_index` (`taggable_id`),
  ADD KEY `tagging_tagged_taggable_type_index` (`taggable_type`),
  ADD KEY `tagging_tagged_tag_slug_index` (`tag_slug`);

--
-- Индексы таблицы `tagging_tags`
--
ALTER TABLE `tagging_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tags_slug_index` (`slug`),
  ADD KEY `tagging_tags_tag_group_id_foreign` (`tag_group_id`);

--
-- Индексы таблицы `tagging_tag_groups`
--
ALTER TABLE `tagging_tag_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tag_groups_slug_index` (`slug`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `action`
--
ALTER TABLE `action`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `filter`
--
ALTER TABLE `filter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `fundamental_settings`
--
ALTER TABLE `fundamental_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tagging_tagged`
--
ALTER TABLE `tagging_tagged`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT для таблицы `tagging_tags`
--
ALTER TABLE `tagging_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `tagging_tag_groups`
--
ALTER TABLE `tagging_tag_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tagging_tags`
--
ALTER TABLE `tagging_tags`
  ADD CONSTRAINT `tagging_tags_tag_group_id_foreign` FOREIGN KEY (`tag_group_id`) REFERENCES `tagging_tag_groups` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
