<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'index.show', 'uses' => 'HomePageController@index'
]);
Route::get('/timer', 'HomePageController@getDateForTimer');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*Posts routes*/
Route::get('/post/{id}', [
   'as' => 'post.show', 'uses' => 'PostController@show'
]);

Route::get('/posts', [
    'as' => 'post.all', 'uses' => 'PostController@showAll'
]);
Route::post('/posts/search', [
    'as' => 'post.search', 'uses' => 'PostController@searchByTag'
]);
/*Comments routes*/
Route::get('/comments', [
    'as' => 'comment.all', 'uses' => 'CommentController@showAll'
]);

Route::post('/comments', [
    'as' => 'comment.store', 'uses' => 'CommentController@store'
]);

Route::get('/contacts', [
    'as' => 'contact.all', 'uses' => 'ContactController@index'
]);

Route::post('/contacts', [
    'as' => 'contact.store', 'uses' => 'ContactController@store'
]);

Route::post('/callback', [
    'as' => 'callback.store', 'uses' => 'HomePageController@callbackStore'
]);

Route::post('/callback-coop', [
    'as' => 'callback-coop.store', 'uses' => 'HomePageController@callbackCoopStore'
]);

Route::get('/vacancies', [
    'as' => 'vacancy.all', 'uses' => 'VacancyController@showAll'
]);
Route::post('/vacancies', [
    'as' => 'vacancy.search', 'uses' => 'VacancyController@search'
]);

Route::get('/services', [
    'as' => 'services.all', 'uses' => 'ServiceController@show'
]);



