<?php

namespace App\Providers;

use App\Comment;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        //\App\User::class => 'App\Http\Sections\Users',
        \App\FundamentalSettings::class => 'App\Http\Sections\FundamentalSettings',
        \App\Post::class => 'App\Http\Sections\Post',
        \App\Vacancy::class => 'App\Http\Sections\Vacancy',
        \App\Comment::class => 'App\Http\Sections\Comment',
        \App\Action::class => 'App\Http\Sections\Action',
        \App\User::class => 'App\Http\Sections\User',

    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
