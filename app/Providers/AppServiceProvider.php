<?php

namespace App\Providers;

use App\FundamentalSettings;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.main', function($view){
            $settings = FundamentalSettings::all();
            $view->with('settings', $settings);
        });
        view()->composer('layouts.contactLayout', function($view){
            $settings = FundamentalSettings::all();
            $view->with('settings', $settings);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
