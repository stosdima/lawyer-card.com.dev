<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $table = 'vacancy';

    protected $fillable = [
        'name', 'category', 'country', 'age', 'expirience', 'sertificate', 'english', 'description',
    ];

    public function language()
    {
        return $this->hasOne('App\Language');
    }

}
