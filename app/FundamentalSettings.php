<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundamentalSettings extends Model
{
    protected $table = 'fundamental_settings';

    protected $fillable = [
        'name', 'var', 'value', 'description',
    ];
}
