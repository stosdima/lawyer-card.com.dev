<?php

namespace App\Http\Sections;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

/**
 * Class Comment
 *
 * @property \App\Comment $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Comment extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $model = '\App\Comment';

    protected $checkAccess = false;


    public function initialize()
    {
        $this->addToNavigation($priority = 600, function(){
            return \App\Comment::count();
        });

        $this->creating(function($config, Model $model){

        });
    }
    /**
     * @var string
     */
    protected $title = 'Комментарии';

    /**
     * @var string
     */
    protected $alias = 'comments';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('full_name', 'Им`я пользователя')->setWidth('300px'),
                AdminColumn::text('body', 'Комментарий')->setWidth('150px'),
                AdminColumn::text('social_link', 'Ссылка на соц.сеть'),
                AdminColumnEditable::checkbox('approve','Подтверждено', 'Не подтверждено')->setLabel('Подтверждение')
            )->paginate(10);
    }

    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::checkbox('approve', 'Подтвердить/Отклонить комментарий?')
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    public function onCreate()
    {
        // todo: remove if unused
    }

    public function isCreatable()
    {
        return false;
    }


    public function isRestorable(Model $model)
    {
        return false;
    }

    public function getIcon()
    {
        return 'fa fa-pencil-square-o';
    }

}
