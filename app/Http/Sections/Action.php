<?php

namespace App\Http\Sections;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

/**
 * Class Action
 *
 * @property \App\Action $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Action extends Section implements Initializable
{

    protected $model = '\App\Action';
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */

    public function initialize()
    {
        $this->addToNavigation($priority = 550, function(){
            return \App\Action::count();
        });

        $this->creating(function($config, Model $model){

        });
    }

    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Акции';

    /**
     * @var string
     */
    protected $alias = 'actions';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('title', 'Название')->setWidth('300px'),
                AdminColumn::text('description', 'Описание'),
                AdminColumn::image('image', 'Фон акции')
                    ->setHtmlAttribute('enctype', 'multipart/form-data'),
                AdminColumn::datetime('date','Дата')->setWidth('150px'),
                AdminColumnEditable::checkbox('enabled','Включено', 'Выключено')->setLabel('Действие')
            )->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название'),
            AdminFormElement::textarea('description', 'Описание'),
            AdminFormElement::upload('image', 'Фон акции'),
            AdminFormElement::date('date', 'Дата'),
            AdminFormElement::checkbox('enabled', 'Включить/Выключить')
        ])->setHtmlAttribute('enctype', 'multipart/form-data');
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function getCreateTitle()
    {
        return 'Создание акции';
    }

    public function getIcon()
    {
        return 'fa fa-thumbs-o-up';
    }
}
