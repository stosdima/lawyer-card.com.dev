<?php

namespace App\Http\Sections;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
/**
 * Class Post
 *
 * @property \App\Post $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Post extends Section implements Initializable
{
    protected $model = '\App\Post';

    public function initialize()
    {
        $this->addToNavigation($priority = 500, function(){
            return \App\Post::count();
        });


        $this->created(function($config, Model $model){
            $post = \App\Post::findOrFail($model->id);
            $post->tag(explode(',', Input::get('tags')));
        });

        $this->updated(function($config, Model $model){
            $post = \App\Post::findOrFail($model->id);
            $post->retag(explode(',', Input::get('tags')));
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Статьи';

    /**
     * @var string
     */
    protected $alias = 'articles';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('title', 'Название статьи')->setWidth('200px'),
                AdminColumn::lists('tags.name', 'Ключевые слова')->setWidth('200px')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название статьи')->required(),
            AdminFormElement::wysiwyg('body', 'Содержимое')->required(),
            AdminFormElement::custom()
                ->setDisplay(function(Model $model){
                    $post = \App\Post::findOrFail($model->id);
                    $tags = implode(',',$post->tagNames());
                    $string = '
                            <div class="form-group form-element-text ">
                                <label for="tags" class="control-label">
                                    Добавить/удалить ключевые слова
                                     <span class="form-element-required">*</span>
                                 </label>'.
                                '<input class="form-control" type="text" id="tags" name="tags"'.' value="'.$tags.'">
                            
                             </div>';
                    return $string;
                }),
            AdminFormElement::text('id', 'ID')->setReadOnly(1),
            AdminFormElement::text('created_at')->setLabel('Создано')->setReadOnly(1),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название статьи')->required(),
            AdminFormElement::ckeditor('body', 'Содержимое')->required(),
            AdminFormElement::html(
                '
                            <div class="form-group form-element-text ">
                                <label for="tags" class="control-label">
                                    Ключевые слова
                                     <span class="form-element-required">*</span>
                                 </label>
                                <input class="form-control" type="text" id="tags" name="tags" placeholder="Введите ключевые слова через запятую">
                             </div>
                 ')
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function getCreateTitle()
    {
        return 'Создание статьи';
    }

    public function getIcon()
    {
        return 'fa fa-book';
    }
}
