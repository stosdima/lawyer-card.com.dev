<?php

namespace App\Http\Sections;

use App\Language;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
/**
 * Class Vacancy
 *
 * @property \App\Vacancy $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Vacancy extends Section implements Initializable
{
    /**
     * @var \App\Vacancy
     */
    protected $model = '\App\Vacany';

    public function initialize()
    {
        $this->addToNavigation($prioriy = '400', function(){
            return \App\Vacancy::count();
        });

        $this->creating(function ($config, Model $model){

        });
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Вакансии';

    /**
     * @var string
     */
    protected $alias = 'vacancies';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('name', 'Им`я')->setWidth('300px'),
                AdminColumn::text('category', 'Категория'),
                AdminColumn::text('age', 'Возраст'),
                AdminColumn::text('country', 'Страна'),
                AdminColumn::text('expirience', 'Опыт работы'),
                AdminColumn::text('sertificate', 'Наличие сертификатов/дипломов')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Им`я')->required(),
            AdminFormElement::select('category', 'Категория')
                ->setEnum(['Для мужчин', 'Для женщин','Для пар', 'Специалисты'])
                ->required(),
            AdminFormElement::select('age', 'Возраст')
                ->setEnum(['От 18 лет', 'От 20 лет', 'До 40 лет'])
                ->required(),
            AdminFormElement::select('country', 'Страна')
                ->setEnum(['Польша', 'Чехия'])
                ->required(),
            AdminFormElement::select('expirience', 'Опыт работы')
                ->setEnum(['Без опыта', '1-5 лет', '5-10 лет', '10-15 лет', '15-20 лет'])
                ->required(),
            AdminFormElement::text('lang_name', 'Язык')->required(),
            AdminFormElement::select('lang_knowledge', 'Уровень владения')
                ->setEnum(['Начальный', 'Средний','Продвинутый'])
                ->required(),
            AdminFormElement::textarea('description', 'Описание')->required(),
            AdminFormElement::checkbox('sertificate', 'Наличие сертификатов/дипломов'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function getCreateTitle()
    {
        return 'Создание вакансии';
    }

    public function getIcon()
    {
        return 'fa fa-tasks';
    }
}
