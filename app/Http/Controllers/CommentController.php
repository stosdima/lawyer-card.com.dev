<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function showAll()
    {
        $comments = Comment::orderBy('id','DESC')->paginate(16);
        return view('review')->with(compact('comments'));
    }

    public function store(Request $request)
    {
        $messages = [
            'full_name.required' => 'Поле Имя и Фамилия обязательно для заполнения',
            'body.required' => 'Поле Ваш отзыв обязательно для заполнения',
            'body.min' => 'Минимальное количество символов: 50',
            'body.max' => 'Максимальное количество символов: 500',
        ];

        $this->validate($request, [
            'full_name' => 'required',
            'body' => 'required|min:50|max:500',

        ], $messages);

        Comment::create($request->all());
        return redirect()->route('comment.all')
            ->with('success', 'Спасибо. Ваш комментарий оставлен!');
    }
}
