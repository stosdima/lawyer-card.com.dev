<?php

namespace App\Http\Controllers;

use App\FundamentalSettings;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        $settings = FundamentalSettings::all();
        return view('contacts')->with(compact('settings'));
    }

    public function store(ContactFormRequest $request)
    {
        Mail::send('emails.contactUs', [
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'question' => $request->get('question'),

        ], function($message){
            $settings = FundamentalSettings::all();
            foreach ($settings as $setting){
                if($setting->name == 'email_notification'){
                    $message->from($setting->value, 'eservice.com.ua');
                    $message->to($setting->value)->subject('Вопрос от клиента');
                }
            }
        });

        return redirect()->route('contact.all')
            ->with('success', 'Спаибо, ваш вопрос был оставлен. Мы свяжемся с вами в течении рабочего дня!');
    }
}
