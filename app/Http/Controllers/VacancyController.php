<?php

namespace App\Http\Controllers;

use App\Language;
use App\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class VacancyController extends Controller
{
    public function showAll()
    {
        $vacancies = Vacancy::orderBy('id', 'DESC')->get();
        $langName = Vacancy::groupBy('lang_name')->pluck('lang_name', 'lang_name');
        return view('vacancy')->with(compact('vacancies', 'langName'));
    }

    public function search()
    {
        $vacancies = Vacancy::where(function($query){
                $query->orWhere('category', '=',  Input::get('category'));
                $query->orWhere('country', '=', Input::get('country'));
                $query->orWhere('age', '=', Input::get('age'));
                $query->orWhere('expirience', '=', Input::get('expirience'));
                $query->orWhere('lang_name', '=', Input::get('lang_name'));
                $query->orWhere('lang_knowledge', '=', Input::get('lang_knowledge'));
                $query->orWhere('sertificate', '=', Input::get('certificate'));
        })
            ->orderBy('id', 'DESC')
            ->get();
        $langName = Vacancy::groupBy('lang_name')->pluck('lang_name', 'lang_name');
        $certificate =Input::get('certificate');

        if(Input::get('category') == '0'
            && Input::get('country') == '0'
            && Input::get('age') == '0'
            && Input::get('expirience') == '0'
            && Input::get('lang_name') == '0'
            && Input::get('lang_knowledge') == '0'
            && !isset($certificate)
            ){
                return redirect()->route('vacancy.all');
        }else{

            if($vacancies->isEmpty()){
                return view('vacancy', compact('vacancies', 'langName'))->with('error', 'Нет соответствующих вакансий по заданым параметрам фильтра');
            }else{
                return view('vacancy', compact('vacancies', 'langName'));
            }
        }

        }

}
