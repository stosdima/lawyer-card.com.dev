<?php

namespace App\Http\Controllers;

use App\Action;
use App\Comment;
use App\FundamentalSettings;
use App\Http\Requests\CallbackCoopForm;
use App\Http\Requests\CallBackFormRequest;
use App\Post;
use App\Vacancy;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class HomePageController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id','DESC')->paginate(7);
        $comments = Comment::orderBy('id','DESC')->paginate(3);
        return view('index')->with(['posts' => $posts, 'comments' => $comments]);

    }

    public function getDateForTimer()
    {
        $arr = [];
        $newDate = [];
        $date = Action::all()
            ->where('enabled', '=', true)
            ->toArray();
        foreach($date as $value){
            $now = Carbon::now();
            $end = Carbon::parse($value['date']);
            $arr[] = $end->diffInDays($now);
        }
        foreach ($date as $value){
            $now = Carbon::now();
            $end = Carbon::parse($value['date']);
            if($end->diffInDays($now) == min($arr)){
                $newDate[]= [
                    'body' => $value['description'],
                    'date' => Carbon::parse($value['date'])->format('Y/m/d'),
                    'href' => $value['image'],
                ];
            }
        }
        return response()->json($newDate);

    }

    public function callbackStore(CallBackFormRequest $request)
    {
        Mail::send('emails.callback', [
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),

        ], function($message){
            $settings = FundamentalSettings::all();
            foreach ($settings as $setting){
                if($setting->name == 'email_notification'){
                    $message->from($setting->value, 'eservice.com.ua');
                    $message->to($setting->value)->subject('Обратный звонок');
                }
            }

        });

        return redirect()->route('index.show')
            ->with('callback', 'Спасибо, ваше заявление на обратный звонок оставлено. Мы свяжемся с вами в течении рабочего дня!');
    }

    public function callbackCoopStore(CallbackCoopForm $request)
    {
        Mail::send('emails.callbackCoop', [
            'name' => $request->get('name'),
            'info' => $request->get('info'),
            'text' => $request->get('text'),

        ], function($message){
            $settings = FundamentalSettings::all();
            foreach ($settings as $setting){
                if($setting->name == 'email_notification'){
                    $message->from($setting->value, 'eservice.com.ua');
                    $message->to($setting->value)->subject('Заявка на сотрудничество');
                }
            }
        });

        return redirect()->route('index.show')
            ->with('callback', 'Спасибо, ваше заявление на сотрудничество оставлено. Мы свяжемся с вами в течении рабочего дня!');
    }

}
