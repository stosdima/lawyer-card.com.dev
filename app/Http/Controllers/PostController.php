<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function showAll(Request $request)
    {
        $posts = Post::orderBy('id','DESC')->paginate(10);
        return view('posts',['posts' => $posts])
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function show($id)
    {
        $post = Post::find($id);
        $readMore = Post::inRandomOrder()->where('id', '<>', $id)->paginate(3);
        return view('single-article', compact('post', 'readMore'));
    }

    public function searchByTag(Request $request)
    {
        if($request->input('keyword')  == ''){

            return redirect()->route('post.all');
        }

        $searchedPosts = Post::orderBy('id','DESC')->withAnyTag($request->input('keyword'))
            ->paginate(10);
        if($searchedPosts->isEmpty()){
            return view('searchedPosts',compact('searchedPosts'))
                ->with('error', 'Нет соответствующих статей по заданом ключевом слову');
        }else{
            return view('searchedPosts',compact('searchedPosts'))
                ->with('i', ($request->input('page', 1) - 1) * 10);
        }

    }
}
