<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use KodiComponents\Support\Upload;

class Action extends Model
{
    use Upload;

    protected $casts = ['image' => 'image'];

    protected $table = 'action';

    protected $fillable = ['title', 'description', 'date'];

    protected $dates = ['date'];

    public function getUploadSettings()
    {
        return [
            'image' => [
                'fit' => [100, 100, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                    }],
                ]
            ];
    }

}
