<?php

Route::get('', ['as' => 'admin.dashboard', function () {
    $user = \Illuminate\Support\Facades\Auth::user()->name;
	$content = 'Добро пожаловать, '.$user.' в панель администратора сайта Eservice.com.ua';
	return AdminSection::view($content, 'Добро пожаловать');
}]);

/*Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);*/