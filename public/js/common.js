$(function() {

    $.getJSON('/timer', function(data){
        if(data.length === 0){
            $(".attention-info").hide();

        }else{
            $.each(data, function(index, value){
                $(".attention-info").css('background', 'url(' + value.href + ') no-repeat 0 50%');
                $(".attention-info").css('background-size','cover');
                $('#countdown').countdown(value.date, function(event) {
                    $(this).html(event.strftime('До конца акции: %w Недель %d Дней  %H:%M:%S'));
                });
                $(".info").text(value.body);
            });
        }
    });


    $(".attention-info i").click(function(){
		$(".attention-info").slideToggle();
	});

	$(".main-menu-button").click(function(){
		$("header ul.dropdown").slideToggle();
	});


	$(".rcall").click().magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});
    $(".forPartners a").click().magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#partName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function() {
                if($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#partName';
                }
            }
        }
    });

	$("#popup__toggle").click().magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

    $(".vacancy-item button").click().magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function() {
                if($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    });

    $(".service-item button").click().magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function() {
                if($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    });

    $('.vacancy-text').readmore({
        maxHeight: 125,
        moreLink: '<a href="#">Развернуть</a>',
        lessLink: '<a href="#">Скрыть</a>'
    });

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail


	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

    $('.review-text').readmore({
        maxHeight: 60,
        moreLink: '<a href="#">Подробнее</a>',
        lessLink: '<a href="#">Скрыть</a>'
    });
	
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});

$("#country").click(function(){
    return false;
});
